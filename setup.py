import setuptools

with open("README.md", "r", encoding = "utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name = "vesty-bmkg",
    version = "0.0.3",
    author = "vesty",
    author_email = "vestiana.aza18@gmail.com",
    description = "tugas vesty",
    long_description = long_description,
    long_description_content_type = "text/markdown",
    url = "https://gitlab.com/vesty/pelatihan_bmkg",
    classifiers = [
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir = {"": "src"},
    packages = setuptools.find_packages(where="src"),
    python_requires = ">=3.8",
    install_requires = [
        "pytest==7.4.0",
        "twine==4.0.2"
      ]
)